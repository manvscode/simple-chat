#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/types.h>
#ifdef WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif
#include <winsock2.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "protocolmessage.h"

void initializeMessage( ProtocolMessage *msg, int type, int dataSize, char *pData )
{
	msg->header.marker = PROTOCOL_MARKER;
	msg->header.type = type;
	msg->header.dataSize = dataSize;
	msg->data = pData;
}

int sendMessage( int socket, ProtocolMessage *msg )
{
	assert( msg != NULL );
	
	// the const_cast here is needed because the data has to be encoded to
	// network byte order (but the data is not changed).
	if( _sendMessage( socket, msg ) == FALSE ) // on failure, handle it...
	{
		
		switch( errno )
		{
			case WSAECONNRESET/*ECONNRESET*/: // connection reset by peer...
				//assert(0);
				break;
			//case EPIPE: // remote connection has dropped...
			//	break;
			//case EINTR: // interrupted by signal...
				//assert(0);
			//	break;
			case EAGAIN:
				return 0;
			default:
				break;
		}
		
		return -1;
	}
	
	return 1;
}

int receiveMessage( int socket, ProtocolMessage *msg )
{
	assert( msg != NULL );
	
	if( _receiveMessage( socket, msg ) < 0 ) // on failure, handle it...
	{
		switch( errno )
		{
			case WSAECONNRESET/*ECONNREFUSED*/: // connection refused...
				break;
			case EINTR:
				break;
			case EAGAIN:
				return 0;
			default:
				break;
		}
			
		return -1;
	}
		
	return 1;
}

int _receiveMessage( int socket, ProtocolMessage *msg ) // no error handling...
{
	int rBytes = 0;
	int rv = 0;
	char *pHeader = (char *) &msg->header;
		
	// receive the message header...
	for( rBytes = 0, rv = 0; rBytes < sizeof(MessageHeader); rBytes += rv )
	{
		if( (rv = recv( socket, pHeader + rBytes, sizeof(MessageHeader) - rBytes, 0 ) ) < 0 )
		{
			if( errno == EINTR || errno == EAGAIN ) continue;
			
			//printf( "ERROR: Header error; received %d of %ld bytes. errno == %d\n", rBytes, sizeof(MessageHeader), errno );
			//perror( "HEADER ERROR" );
			return -1;
		}
		else if( rv == 0 )
			return 0; // no error; orderly shutdown...
	}
		
	// convert to message header to host order...
	msg->header.marker = ntohl( msg->header.marker );
	msg->header.type = ntohl( msg->header.type );
	msg->header.dataSize = ntohl( msg->header.dataSize );
		
	if( msg->header.marker != PROTOCOL_MARKER )
	{		
		//printf( "MARKER ERROR: Incoming packet had bad marker! Packet will be discarded.\n" );
		return FALSE;
	}
		
	if( msg->header.dataSize > 0 )
	{
		// allocate memory for data...
		msg->data = (char *) g_malloc( sizeof(char) * msg->header.dataSize );
		
		// receive the data...
		for( rBytes = 0, rv = 0; rBytes < msg->header.dataSize; rBytes += rv )
		{
			if( (rv = recv( socket, msg->data + rBytes, msg->header.dataSize - rBytes, 0 ) ) < 0 )
			{
				if( errno == EINTR || errno == EAGAIN ) continue;
			
				//perror( "DATA ERROR" );
				g_free( msg->data );
				return -1;
			}
			else if( rv == 0 )
			{
				g_free( msg->data );
				return 0; // no error; orderly shutdown...
			}
		}
	}
	#ifdef _DEBUG
		int i;
		g_printf( "DEBUG: Received MSG %.8x [", msg->header.type );
		for( i = 0; i < msg->header.dataSize; i++ )
			g_printf( "%c", ( msg->data[ i ] == '\0' ? '&' : msg->data[ i ] ) ); 
		g_printf( "] (size = %u).\n", msg->header.dataSize );
	#endif
	return 1;
}
int _sendMessage( int socket, ProtocolMessage *msg ) // no error handling...
{
	assert( msg->header.type != 0 );
	assert( msg->header.dataSize >= 0 );
	#ifdef _DEBUG
		int i;
		g_printf( "DEBUG: Sending MSG %.8x [", msg->header.type );
		for( i = 0; i < msg->header.dataSize; i++ )
			g_printf( "%c", ( msg->data[ i ] == '\0' ? '&' : msg->data[ i ] ) ); 
		g_printf( "] (size = %u).\n", msg->header.dataSize );
	#endif

	int dataSize = msg->header.dataSize;
	// convert to message header to network order...
	msg->header.marker = htonl( PROTOCOL_MARKER );
	msg->header.type = htonl( msg->header.type );
	msg->header.dataSize = htonl( msg->header.dataSize );
	
	int size = sizeof(MessageHeader);
	int count = 0;
		
	// send the header...
	while( size > 0 )
	{
		int sentBytes = send( socket, (const char *) &msg->header + count, size, 0 );
		
		if( sentBytes <= 0 )
		{
			if( errno == EINTR || errno == EAGAIN ) continue;
			
			//printf( "ERROR: Header error; sent %d of %d bytes. errno == %d\n", sentBytes, size, errno );
			return FALSE;
		}
		
		size -= sentBytes;
		count += sentBytes;
	}
	
	count = 0;

	while( dataSize > 0 )
	{
		// send the data...
		int sentBytes = send( socket, msg->data + count, dataSize, 0 );
		
		if( sentBytes <= 0 )
		{
			if( errno == EINTR || errno == EAGAIN ) continue;
			
			//printf( "ERROR: Data error; only sent %d of %d bytes. errno == %d\n", sentBytes, dataSize, errno );
			return FALSE;
		}
		
		dataSize -= sentBytes;
		count += sentBytes;
	}
		
	return TRUE;
}

int resolveName( const char *pName, unsigned long *address )
{
	struct hostent *host;
		
	if( (host = gethostbyname( pName ) ) == NULL )
		return FALSE;
	
	*address = *( (unsigned long *) host->h_addr_list[ 0 ] );
	return TRUE;	
}


#ifdef _DEBUG
void debugString( const char *s, unsigned int length )
{
	unsigned int i;
	printf( "DEBUG string [" );
	for( i = 0; i < length; i++)
		printf( "%c", ( s[ i ] == '\0' ? '&' : s[ i ] ) );
	printf( "] (string length = %d)\n", length );
}
#endif
