#ifndef _PROTOCOLMESSAGE_H_
#define _PROTOCOLMESSAGE_H_
/***************************************************************************
 *            ServerMessage.h
 *
 *  Tue Sep  4 02:32:29 2007
 *  Copyright  2007  Joseph A. Marrero
 *  joe@L33Tprogrammer.com
 ****************************************************************************/

#include <stdio.h>

#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif
#define __private static
typedef unsigned char byte;

	
#define MAX_MESSAGE_TYPES					32
#define BYTES_FOR_MESSAGE_TYPE				((MAX_MESSAGE_TYPES) >> 3)
#define MAX_PAYLOAD_SIZE					1024
#define PROTOCOL_MARKER						0xFFEF

/*
 * 	Protocol Definition
 *
 *	This section defines message types and the
 *	associated parameters.
 */


#define MT_NO_MESSAGE						0x00000000
#define MT_USER_ENTER						0x00000001				// user name (client)
#define MT_USER_LEAVE						0x00000002				// NIL (client)
#define MT_CHATROOM_LIST					0x00000003				// NIL (client), list of chatrooms (server)
#define MT_USER_LIST						0x00000004				// chatroom (client), list of username@ip (server)
#define MT_ENTER_CHATROOM					0x00000005				// chatroom name (client)
#define MT_LEAVE_CHATROOM					0x00000006				// chatroom name (client)
#define MT_SEND_CHATROOM_MESSAGE			0x00000007				// chatroom name and message (client), username, chatroom and message (server)
#define MT_SERVER_CHATROOM_MESSAGE			0x00000008				// message (server)
#define MT_SEND_USER_MESSAGE				0x00000009				// username and message (client), other user name and message (server)
#define MT_NOTIFY_ERROR						0x0000000A				// error message (server)
#define MT_NOTIFY_USER_JOINED				0x0000000B				// Chatroom Username IP (server)
#define MT_NOTIFY_USER_LEFT					0x0000000C				// Chatroom Username@IP (server)

/*
 * 	Message Definition
 *
 *	This section defines the structure of
 *	protocol messages.
 */
#pragma pack(push, 1) // align to 2 bytes
	typedef struct tagMessageHeader {
		int marker; // = 0xFFEF;
		int type;
		int dataSize;
	} MessageHeader;
	
	typedef struct tagProtocolMessage {
		MessageHeader header;
		char *data;
	
	} ProtocolMessage;
#pragma pack(pop)
	
void initializeMessage( ProtocolMessage *msg, int type, int dataSize, char *pData );
	
int sendMessage( int socket, ProtocolMessage *msg );
int receiveMessage( int socket, ProtocolMessage *msg );	
/*__private*/ int _receiveMessage( int socket, ProtocolMessage *msg ); // no error handling...
/*__private*/ int _sendMessage( int socket, ProtocolMessage *msg ); // no error handling...
#define freeMessageData( pMsg )		(g_free( (pMsg)->data ))
	
	
int resolveName( const char *pName, unsigned long *address );
	
#ifdef _DEBUG
void debugString( const char *s, unsigned int length );
#endif	
	
#endif
