#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <glib.h>
#include <glib/gprintf.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "simplechat.h"


void on_connect_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	pthread_t connectThreadID;
	
	if( pthread_create( &connectThreadID, NULL, connectThread, NULL ) != 0 )
	{
		displayError( NULL, "Failed to create connection thread!", TRUE );
	}
}

void on_disconnect_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	pthread_t disconnectThreadID;
	
	if( pthread_create( &disconnectThreadID, NULL, disconnectThread, NULL ) != 0 )
	{
		displayError( NULL, "Failed to create disconnection thread!", TRUE );
	}
}

void on_settings_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	settingsDlg( );
}

void on_quit_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	gtk_main_quit( );
}


void on_about_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	GtkWidget *pAboutDlg = create_aboutDlg( );
	gtk_dialog_run( GTK_DIALOG( pAboutDlg ) );
	gtk_widget_destroy( pAboutDlg );
}

void on_userList_row_activated( GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data )
{

}

void on_toolbuttonConnect_clicked( GtkToolButton *toolbutton, gpointer user_data )
{
	pthread_t connectThreadID;
	
	if( pthread_create( &connectThreadID, NULL, connectThread, NULL ) != 0 )
	{
		displayError( NULL, "Failed to create connection thread!", TRUE );
	}
}

void on_toolbuttonDisconnect_clicked( GtkToolButton *toolbutton, gpointer user_data )
{
	pthread_t disconnectThreadID;
	
	if( pthread_create( &disconnectThreadID, NULL, disconnectThread, NULL ) != 0 )
	{
		displayError( NULL, "Failed to create disconnection thread!", TRUE );
	}
}

void on_sendButton_pressed( GtkButton *button, gpointer user_data )
{
	if( nSocket == -1 )
	{
		displayError( GTK_WINDOW( mainWindow ), "Please connect first.", TRUE );
		return;
	}
	
	GtkEntry *pSendTextEntry = GTK_ENTRY( lookup_widget( mainWindow, "sendTextEntry" ) );
	
	const gchar *pText = gtk_entry_get_text( pSendTextEntry );
	sendChatroomMessageToServer( &chatroomInfo.chatroomName[ 0 ], pText );
	
	gtk_entry_set_text( pSendTextEntry, "" );
}

void on_toolbuttonSettings_clicked( GtkToolButton *toolbutton, gpointer user_data )
{
	settingsDlg( );
}

void on_toolbuttonAbout_clicked( GtkToolButton *toolbutton, gpointer user_data )
{
	GtkWidget *pAboutDlg = create_aboutDlg( );
	gtk_dialog_run( GTK_DIALOG( pAboutDlg ) );
	gtk_widget_destroy( pAboutDlg );
}


gboolean on_sendTextEntry_key_press_event( GtkWidget *widget, GdkEventKey *event, gpointer user_data )
{
	if( event->keyval == GDK_Return )
	{
		if( !isConnected( ) )
		{
			displayError( GTK_WINDOW( mainWindow ), "Please connect first.", TRUE );
			return TRUE; // looks weird but correct
		}

		on_sendButton_pressed( NULL, NULL );

		debug( "enter hited!\n" );
	}

	return FALSE;
}

void on_mainWindow_show                     (GtkWidget       *widget,
                                        gpointer         user_data)
{

}

void on_itemJoinChatroom_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	joinChatroomDlg( );
}


void on_itemLeaveChatroom_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	leaveChatroom( TRUE );
}

void on_toolbuttonJoinChatroom_clicked( GtkToolButton *toolbutton, gpointer user_data )
{
	joinChatroomDlg( );
}

void on_toolbuttonLeaveChatroom_clicked( GtkToolButton *toolbutton, gpointer user_data )
{
	leaveChatroom( TRUE );
}

gboolean on_mainWindow_delete_event( GtkWidget *widget, GdkEvent *event, gpointer user_data )
{
	gtk_main_quit( );
  	return FALSE;
}
