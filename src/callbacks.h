#include <gtk/gtk.h>


void
on_connect_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_disconnect_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_settings_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_okbutton_pressed                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_userList_row_activated              (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data);

void
on_toolbuttonConnect_clicked           (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_toolbuttonDisconnect_clicked        (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_sendButton_pressed                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_toolbuttonSettings_clicked          (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_toolbuttonAbout_clicked             (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_sendTextEntry_key_press_event       (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_mainWindow_activate_default         (GtkWindow       *window,
                                        gpointer         user_data);

void
on_mainWindow_show                     (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_itemJoinChatroom_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_itemLeaveChatroom_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_toolbuttonJoinChatroom_clicked      (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_toolbuttonLeaveChatroom_clicked     (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

gboolean
on_mainWindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);
