#ifndef _MAIN_H_
#define _MAIN_H_
/***************************************************************************
 *            main.h
 *
 *  Wed Sep  5 07:42:48 2007
 *  Copyright  2007  Joseph A. Marrero
 *  joe@L33Tprogrammer.com
 ****************************************************************************/
#include <string>
#include <gtk/gtk.h>
#include <pthread.h>
#include "protocol.h"
using namespace std;
using namespace NetMessaging;

#define PORT                          7575
#define CHATROOM_LIST_ATTEMPTS        5

typedef basic_string<gchar> gstring;


extern GtkWidget *mainWindow;

void initializeApplication( );
void deinitializeApplication( );
#ifdef WIN32
void initializeWinsock( );
#endif

typedef struct tagSettings {
	gstring serverHostname;
	unsigned int nPort;
	unsigned int nConnectionAttempts;
	gstring username;
} Settings;

extern Settings settings;


typedef struct tagChatroomInfo {
	gstring chatroomName;
	gboolean bListening;
	GtkListStore *pUserListStore; // list of usernames and IPs
} ChatroomInfo;

extern ChatroomInfo chatroomInfo;

enum { COL_USERNAME = 0, COL_IPADDRESS = 1 };

enum { JOIN, LEFT };
#define INFO_PREFIX		("::::: ")

void initializeSettings( Settings *settings );
void settingsDlg( );
void settingsDlgUpdate( GtkWidget *pSettingsDlg, int bToUpdateControls );

extern int nSocket;

void *connectThread( void *args );
void *disconnectThread( void *args );
bool doConnect( );
void doDisconnect( gboolean updateUI );
bool isConnected( );

void joinChatroomDlg( );
void joinChatroom( );
void leaveChatroom( gboolean bUpdateUI );
void *leaveChatroomThread( void *args );
void _leaveChatroom( gboolean bUpdateUI );

void displayError( GtkWindow *pParent, const gchar *error, gboolean bWithinGtkCallback );
void displayInfo( GtkWindow *pParent, const gchar *info, gboolean bWithinGtkCallback );
gint displayQuestion( GtkWindow *pParent, const gchar *question, gboolean bWithinGtkCallback );

void sigPIPEHandler( int signal );
void sigTERMHandler( int signal );

void *listeningThread( void *args );
int handleMessage( Protocol::Message *msg );
int handleIncomingChatroomMessage( Protocol::Message *msg );
int handleIncomingChatroomServerMessage( Protocol::Message *msg );
int handleIncomingUserList( Protocol::Message *msg );


void updateChatTextViewWithUserMessage( GtkWidget *pParent, gchar *username, gchar *text );
void updateChatTextViewWithInfo( GtkWindow *pParent, gchar *info, gboolean bWithinGtkCallback );

void sendChatroomMessageToServer( const gchar *pChatroom, const gchar *pMessage );

void requestUserListFromServer( gchar *chatroomName, unsigned int chatroomNameLength );
void setUserList( gchar *pUserList, unsigned int nUserListLength );
int handleNotifyUserJoinedLeft( Protocol::Message *msg, int joinedOrLeft );
void clearUserList( );
void setTitle( );
void restoreTitle( );

void setStatusBarText( const gchar *text, gboolean bWithinGtkCallback );



#ifdef _DEBUG
void debug( const char *str, ... );
void debugString( const char *s, unsigned int length );
#else
inline void debug( const char *str, ... ){}
inline void debugString( const char *s, unsigned int length){}
#endif

void setSocketOptions( int socket, bool bOn );

#endif
